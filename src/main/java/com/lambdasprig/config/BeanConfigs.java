package com.lambdasprig.config;

import com.lambdasprig.employee.EmployeeFunction;
import com.lambdasprig.employee.EmployeeInput;
import com.lambdasprig.employee.EmployeeOutput;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfigs {

    @Bean
    public EmployeeOutput getEmployeeList(){
        return new EmployeeOutput();
    }

    @Bean
    public EmployeeFunction getEmployeeByName(){
        return new EmployeeFunction();
    }

    @Bean
    public EmployeeInput saveEmployee(){
        return new EmployeeInput();
    }
}
