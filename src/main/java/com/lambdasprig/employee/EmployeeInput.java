package com.lambdasprig.employee;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.function.Consumer;

@Component
public class EmployeeInput implements Consumer<Employee> {

    @Autowired
    private EmployeeLoad employeeLoad;

    @Override
    public void accept(Employee employee) {
        employeeLoad.employeesList()
                .add(new Employee(employee.getId(), employee.getName(), employee.getBorn(), employee.getDepartment(), employee.getSalary()));
        System.out.println(employeeLoad.employeesList());
    }
}
