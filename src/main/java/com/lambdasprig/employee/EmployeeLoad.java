package com.lambdasprig.employee;

import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Repository
public class EmployeeLoad {

    public List<Employee> employeesList(){
        return Stream.of(
                new Employee(1L, "Alvaro", LocalDate.of(2000,1,24), "TI", new BigDecimal(7000)),
                new Employee(2L, "spring.cloud.function.definition", LocalDate.of(1998,5,12), "Market", new BigDecimal(5000)),
                new Employee(3L, "Gustavo", LocalDate.of(1995,10,23), "Sales", new BigDecimal(3000)),
                new Employee(4L, "Chica", LocalDate.of(1999,2,19), "Market", new BigDecimal(4000))
        ).collect(Collectors.toList());
    }

}
