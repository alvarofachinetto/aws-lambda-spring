package com.lambdasprig.employee;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class EmployeeFunction implements Function<String, List<Employee>> {

    @Autowired
    private EmployeeLoad employeeLoad;

    @Override
    public List<Employee> apply(String name) {
        return employeeLoad.employeesList().stream().filter(employee -> employee.getName().equals(name)).collect(Collectors.toList());
    }
}
