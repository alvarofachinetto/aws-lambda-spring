package com.lambdasprig.employee;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.function.Supplier;

@Component
public class EmployeeOutput implements Supplier<List<Employee>> {

    @Autowired
    private EmployeeLoad employeeLoad;

    @Override
    public List<Employee> get() {
        return employeeLoad.employeesList();
    }
}
